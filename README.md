# Install Docker

    sudo apt install docker.io

    Note : The 'docker' parameter is group created upon installing docker.  Jenkins user cannot use docker command. 
           Add jenkins to docker group

   > getent group | grep docker 
    

   > sudo usermod -aG docker jenkins
   
   Restart the machine to get the changes reflected.

# Build docker image

    sudo docker build -t travel_agency . 


# Run docker container
    docker run -it --rm -p 8085:8080 --name Travel-Agency.war travel_agency [Foreground]
    docker container run -d -p 8083:8080 travel_agency [Background]
    curl http://localhost:8085/Travel-Agency/
    
# clean up docker iamge & container resources
    docker system prune -a
    
    
    
    

